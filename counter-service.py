 #!flask/bin/python
from flask import Flask, request, request_started

app = Flask(__name__)
counterPost = 0
counterGet = 0
@app.route('/', methods=["POST", "GET"])
def index():
    global counterPost, counterGet
    if request.method == "POST":
        counterPost+=1
        return "Hmm, Plus 1 please "
    else:
        counterGet+=1
        return str(f"<p>Our counter for POST method is: <b>{counterPost}</b></p>  <p>Our counter for GET method is: <b>{counterGet}</b></p>")

if __name__ == '__main__':
    app.run(debug=True,port=80,host='0.0.0.0')
